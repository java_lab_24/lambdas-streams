package controller;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import model.command.SayAble;
import model.command.SayAbleImpl;

public class SayAbleController {

  private SayAbleImpl sayAble;
  private Map<String, SayAble> commands;

  public SayAbleController() {
    sayAble = new SayAbleImpl();
    commands = new LinkedHashMap<>(3);
    commands.put("CONGRATS", sayAble.getCongratulator());
    commands.put("ASK", sayAble.getInterrogator());
    commands.put("ANSWER", sayAble.getAnswerer());
  }

  public Set<String> getCommands() {
    return commands.keySet();
  }

  public void execute(String command, String msg) {
    command = command.toUpperCase();
    try {
      commands.get(command).say(msg);
    } catch (NullPointerException e) {
      throw new IllegalArgumentException("Unknown command");
    }
  }

}
