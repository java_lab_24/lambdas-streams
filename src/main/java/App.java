import java.util.ArrayList;
import view.MainView;

public class App {

  public static void main(String[] args) {
    new MainView().show();
  }
}
