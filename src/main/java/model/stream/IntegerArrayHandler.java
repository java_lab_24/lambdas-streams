package model.stream;

import java.util.stream.Stream;

public class IntegerArrayHandler {

  public String getIntSummaryStatistics(Integer[] integers) {
    Stream<Integer> stream = Stream.of(integers);
    return stream.mapToInt((x) -> x)
        .summaryStatistics()
        .toString();
  }

  public double getAverage(Integer[] integers) {
    Stream<Integer> stream = Stream.of(integers);
    return stream.mapToInt(x -> x).average().orElse(-1);
  }

  public long getNumOfElementsBiggerThenAverage(Integer[] integers) {
    Stream<Integer> stream = Stream.of(integers);
    double average = getAverage(integers);
    return stream.filter(i -> i > average).count();
  }
}
