package model.stream;

import java.util.Random;
import java.util.stream.Stream;

public class IntegerSequenceGenerator {

  public Integer[] getIntegers(int arrayLength) {
    Random random = new Random();
    return Stream
        .generate(random::nextInt)
        .limit(arrayLength)
        .toArray(Integer[]::new);
  }

  public Integer[] getPositiveIntegers(int arrayLength) {
    Random random = new Random();
     return Stream
         .generate(random::nextInt)
         .filter(i -> i > 0)
         .limit(arrayLength)
         .toArray(Integer[]::new);
    }
}
