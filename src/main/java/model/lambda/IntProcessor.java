package model.lambda;

@FunctionalInterface
public interface IntProcessor {

  int process(int i1, int i2);
}
