package model.lambda;

public class IntProcessorImpl {

  IntProcessor getMax;
  IntProcessor getAverage;

  public IntProcessorImpl(){
    getMax = (i1, i2) -> i1 > i2 ? i1 : i2;
    getAverage = (i1, i2) -> (i1 + i2) / 2;
  }

  public IntProcessor getMax() {
    return getMax;
  }

  public IntProcessor getAverage() {
    return getAverage;
  }
}
