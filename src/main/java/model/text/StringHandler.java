package model.text;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StringHandler {

  public long countUniqueWords(String[] words) {
    Stream<String> stream = Stream.of(words);
    return stream
        .map(String::toLowerCase)
        .collect(Collectors.groupingBy(w -> w, Collectors.counting()))
        .entrySet().stream()
        .filter(e -> e.getValue() == 1)
        .count();
  }

  public List<String> getSortedListOfUniqueWords(String[] words) {
    Stream<String> stream = Stream.of(words);
    return stream
        .map(String::toLowerCase)
        .collect(Collectors.groupingBy(w -> w, Collectors.counting()))
        .entrySet().stream()
        .filter(e -> e.getValue() == 1)
        .map(e -> e.getKey())
        .sorted()
        .collect(Collectors.toList());
  }

  public Map<String, Long> getWordsFrequency(String[] words) {
    Stream<String> stream = Stream.of(words);
    return stream
        .map(String::toLowerCase)
        .collect(Collectors.groupingBy(w -> w, Collectors.counting()));
  }

  public String[] buildWords(List<String> lines) {
    return lines.stream()
        .flatMap(l -> Arrays.stream(l.split(" ")))
        .toArray(String[]::new);
  }
}
