package model.command;

public class SayAbleImpl implements SayAble {

  private SayAble congratulator;
  private SayAble interrogator;
  private SayAble answerer;

  public SayAbleImpl() {
    congratulator = (msg) -> System.out.println("congratulator says: -" + msg);
    interrogator = new SayAble() {
      @Override
      public void say(String msg) {
        System.out.println("interrogator says: -" + msg);
      }
    };
    answerer = SayAbleImpl::answererExecutor;
  }

  private static void answererExecutor(String msg) {
    System.out.println("answerer says: -" + msg);
  }

  public SayAble getCongratulator() {
    return congratulator;
  }

  public SayAble getInterrogator() {
    return interrogator;
  }

  public SayAble getAnswerer() {
    return answerer;
  }

  @Override
  public void say(String msg) {
    System.out.println(msg);
  }
}
