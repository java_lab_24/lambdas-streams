package model.command;

@FunctionalInterface
public interface SayAble {

  void say(String msg);
}
