package view;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import model.lambda.IntProcessorImpl;
import model.stream.IntegerArrayHandler;
import model.stream.IntegerSequenceGenerator;
import model.text.StringHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MainView {

  private static final Logger logger = LogManager.getLogger(MainView.class);
  private static Scanner input = new Scanner(System.in);
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private IntProcessorImpl intProcessor;
  private CommandsView commandsView;
  private IntegerSequenceGenerator integerSequenceGenerator;
  private IntegerArrayHandler integerArrayHandler;
  private StringHandler stringHandler;

  public MainView() {
    menu = new LinkedHashMap<>();
    menu.put("1", "  1 - Execute Lambda functions which processes integers");
    menu.put("2", "  2 - Execute commands");
    menu.put("3", "  3 - Test Integers stream");
    menu.put("4", "  4 - Test text processor");
    menu.put("q", "  q - exit");

    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::pressButton1);
    methodsMenu.put("2", this::pressButton2);
    methodsMenu.put("3", this::pressButton3);
    methodsMenu.put("4", this::pressButton4);

    intProcessor = new IntProcessorImpl();
    commandsView = new CommandsView();
    integerSequenceGenerator = new IntegerSequenceGenerator();
    integerArrayHandler = new IntegerArrayHandler();
    stringHandler = new StringHandler();
  }

  private void pressButton1() {
    System.out.print("\nPlease enter two integers: ");
    int int1 = input.nextInt();
    int int2 = input.nextInt();
    logger.trace("Got two integers " + int1 + " and " + int2);
    logger.trace("Sending input data to getAverage and getMax Lambda functions");
    logger.info("getAverage lambda result :" + intProcessor.getAverage().process(int1, int2));
    logger.info("getMax lambda result :" + intProcessor.getMax().process(int1, int2));
    input = new Scanner(System.in);
  }

  private void pressButton2() {
      commandsView.show();
  }

  private void pressButton3() {
    logger.info("Enter length of integer array: ");
    int arrayLength = input.nextInt();
    Integer[] integerArray = integerSequenceGenerator.getIntegers(arrayLength);
    logger.trace("Generated Integer Array: {" + Arrays.toString(integerArray) + "}");
    Integer[] positiveIntegerArray = integerSequenceGenerator.getPositiveIntegers(arrayLength);
    logger.trace("Generated Positive Integer Array: {" + Arrays.toString(positiveIntegerArray) + "}");
    logger.info("Statistic of Integer Array: "
        + integerArrayHandler.getIntSummaryStatistics(integerArray)
        + "\nNumber of elements bigger then average: "
        + integerArrayHandler.getNumOfElementsBiggerThenAverage(integerArray));
    logger.info("Statistic of Positive Integer Array: "
        + integerArrayHandler.getIntSummaryStatistics(positiveIntegerArray)
        + "\nNumber of elements bigger then average: "
        + integerArrayHandler.getNumOfElementsBiggerThenAverage(positiveIntegerArray));
  }

  private void pressButton4() {
    logger.info("Enter your text: ");
    List<String> lines = new LinkedList<>();
    String line;
    while ((line = input.nextLine()).length() > 0){
      lines.add(line);
    }
    String[] words = stringHandler.buildWords(lines);
    logger.info("Number of unique words in text:" + stringHandler.countUniqueWords(words));
    logger.info("Sorted list of all unique words:" + stringHandler.getSortedListOfUniqueWords(words));
    logger.info("Words frequency: " + stringHandler.getWordsFrequency(words));
  }

  private void outputMenu() {
    System.out.println("\nMENU:");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  public void show() {
    System.out.println("Task04. Lambdas. Streams");
    String keyMenu;
    do {
      outputMenu();
      System.out.print("Please, select menu option: ");
      keyMenu = input.nextLine();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("q"));
  }

}
