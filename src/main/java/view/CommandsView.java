package view;

import controller.SayAbleController;
import java.util.Scanner;

public class CommandsView {

  private static Scanner input = new Scanner(System.in);
  private SayAbleController controller;

  public CommandsView() {
    controller = new SayAbleController();
  }

  private void outputMenu() {
    System.out.println("\nSayAble commands:");
    System.out.println("\t[SAY], " + controller.getCommands());
    System.out.println("\tq - go back to main menu");
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.print("Please, enter command to execute: ");
      keyMenu = input.nextLine();
      if (keyMenu.equals("q")) {
        break;
      }
      System.out.print("Please, enter message: ");
      String msg = input.nextLine();
      try {
        controller.execute(keyMenu, msg);
      } catch (Exception e) {
        System.err.println(e);
      }
    } while (!keyMenu.equals("q"));
  }
}
